const keys = require('../keys');
const test_object = require('../extra_var');
test("Testing keys function", () => {
    expect(keys(test_object)).toEqual(['name', 'age', 'location']);
})