const defaults = require('../defaults');
const test_object = require("../extra_var");
test("Testing Defaults Function", () => {
    defaults(test_object, { location: 'metrocity', arch_nemesis: "Joker", ally: "Superman" });
    expect(test_object).toEqual({ name: 'Bruce Wayne', age: 36, location: 'Gotham', arch_nemesis: "Joker", ally: "Superman" })
});