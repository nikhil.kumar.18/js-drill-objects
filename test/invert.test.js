const test_object = require("../extra_var");
const invert = require("../invert");
test("Testing invert function", () => {
    expect(invert(test_object)).toEqual({ "Bruce Wayne": "name", "36": "age", 'Gotham': 'location' })
})