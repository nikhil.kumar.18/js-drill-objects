const values = require('../values');
const test_object = require('../extra_var');
test("Testing values function", () => {
    expect(values(test_object)).toEqual(['Bruce Wayne', 36, 'Gotham']);
})