const map_object = require('../map_object');
const test_object = require('../extra_var');

function robin(val) {
    return val + ' robin';
}

test("Testing map_object function", () => {
    map_object(test_object, robin);

    expect(test_object).toEqual({ name: 'Bruce Wayne robin', age: '36 robin', location: 'Gotham robin' })
})
