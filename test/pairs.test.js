const pairs = require('../pairs');
const test_object = require('../extra_var');
test("Testing Pairs function", () => {
    expect(pairs(test_object)).toEqual([['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']])
})