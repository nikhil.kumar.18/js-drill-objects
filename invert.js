function invert(obj) {
    const copy_obj = {};
    for (let keys in obj) {
        copy_obj[obj[keys]] = keys;
    }
    return copy_obj;
}
module.exports = invert;