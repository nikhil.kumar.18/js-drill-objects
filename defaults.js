function defaults(obj, default_val) {
    for (let key in default_val) {
        if (obj[key] === undefined) {
            obj[key] = default_val[key];
        }
    }
}
module.exports = defaults;