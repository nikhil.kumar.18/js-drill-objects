function pairs(obj) {
    let list_pair = [];
    for (let keys in obj) {
        list_pair.push([keys, obj[keys]]);
    }
    return list_pair;
}
module.exports = pairs;