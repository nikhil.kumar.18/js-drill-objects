function map_object(obj, func) {
    for (let keys in obj) {
        obj[keys] = func(obj[keys]);
    }
    //no need to return changes would be made in original object
}

module.exports = map_object;